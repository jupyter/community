FROM rust:1-slim
ARG MDBOOK_VERSION="0.4.3"

RUN cargo install mdbook --vers ${MDBOOK_VERSION}
RUN cargo install mdbook-mermaid

WORKDIR /data
VOLUME ["/data"]
