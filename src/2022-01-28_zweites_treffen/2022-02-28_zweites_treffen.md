# Jupyter Community Treffen am 28.01.2022
(Zoom Veranstaltung)    
Infos, Folien, Protokoll sind hier zu finden:  
[https://blog.rwth-aachen.de/itc-events/event/rwthjupyter-community-treffen](https://blog.rwth-aachen.de/itc-events/event/rwthjupyter-community-treffen)


## Tagesordnung:

**RWTHjupyter**
 - Stand des Services RWTHjupyter, Auslastungsdaten
-  Maintenance und Updates 2021/2022
-  GPU Nutzung
-  Aktuelle Projekte	
-   SLEW
-   Jupyter4Power (eduGain Integration)

**JupyterHub für RWTH High-Performance Computing**

**RWTHjupyter Community**
-  Vorstellung des Community Beteiligungsprozesses
-  Nutzendentreffen

**Fragen / Diskussion**
 
