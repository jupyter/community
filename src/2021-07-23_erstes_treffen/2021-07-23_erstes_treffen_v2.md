# Jupyter Community Treffen am 23.07.2021
(Zoom Veranstaltung)

## Vorstellung RWTHjupyter und RWTHjupyter Community
- Vortragende: Georg Schramm, Stefan Lankes, Steffen Vogel, Dörte Rosendahl, Christian Rohlfing
- siehe [https://git.rwth-aachen.de/jupyter/community/-/blob/master/src/2021-07-23_erstes_treffen/2021-07-23_erstes_treffen_rwth_jupyter_community.pdf](https://git.rwth-aachen.de/jupyter/community/-/blob/master/src/2021-07-23_erstes_treffen/2021-07-23_erstes_treffen_rwth_jupyter_community.pdf)
  - Entwicklung des Services RWTHjupyter
  - Use Cases 
  - RWTHjupyter Community

## Fragen und Diskussion:


- Frage: Wie funktioniert die Ablage in GitLab, wenn schon nach 1 Jahr Projekte gelöscht werden. (Anmerkung: "treibt die Leute zu GitHub")
  - Antwort: Projekte können archiviert werden, ansonsten wird nach einer anderen Lösung gesucht werden
- Frage: Ist NRW-weite Nutzung von RWTHjupyter möglich?
  - Antwort: sollte demnächst über DFN/AAI (Edugain) möglich sein. Zuordnung Mandantenfähigkeit wird in RWTHjupyter realisiert
- Frage: Ist in Moodle nur 1 Jupyter Profil möglich?
  - Antwort: es sind mehrere Profile möglich, aber es besteht die Gefahr, dass dann die Umgebung nicht stimmt
- Anmerkung: Kooperation gewünscht, aber keine Möglichkeit im Institut Testumgebung für Entwicklungen (Weiterentwicklung von Features z.B. Feedbackfunktion) aufzusetzen.
  - Antwort: Wir setzen gerade Testumgebung auf, die ggf. genutzt werden könnte. Zudem ist alles Open Source, von daher ist es auch möglich eigene Setups aufzubauen
- Anmerkung: Die Community Veranstaltung soll Aufruf sein den RWTHjupyter Service zu gestalten

---------------
## Weitere Rückmeldungen:

 
- Danke für den Service, Featurewunsch: Rollenverteilungen in Jupyter, Zugriff auf nbgrader (Vereinfachung des Zugangs)
  - Teilnehmer-Abstimmung ergibt 1/3 Interesse 
  - Anmerkung: technisch bisher nicht realisierbar, mit JupyterLab nun denkbar, Aufruf an RWTH hier mitzuwirken, evtl. auch in allgemeiner Jupyter Community schauen, da auch von anderen Unis gewünscht (Kooperation?)
- Einsatz von RWTHjupyter wichtig  (Beispiel Machine Learning)
  - Wunsch RWTHweit Erfahrungen zusammenzutragen (Umsetzung von Features, z.B. Moodleanbindung)
  - Teilnehmer-Abstimmung ergibt 8 Stimmen
- Lehrstuhl im Bereich Architektur - machen viele Abgaben, Timestamps möglich? Einladung auf Notebook eines Studis möglich? Einblick in Aktivität gewünscht, (Learning activity) dann evt. Zuschaltung über Zoom möglich?
  - Abgabe ggf. über Moodle vorstellbar (Workflow in Moodle)
  - IT Center weist auf steigende Relevanz bei Einsatz für Prüfungen etc. möglich (Verfügbarkeit...), sollte im Forum ggf. definiert werden.
- Anregung zum Thema Zugang zu den Notebooks (Dozent schickt aktuell den Studierenden einen Link): 
  - optische Gestaltung der Notebookübersicht verbessern und Linkliste zu eigenem Projekt anlegen
- Externe Nutzung bzw. Nutzung durch SchülerInnen, wie Zugang ermöglichen? (Pseudoaccounts)
  - IT Center: aktuell noch nicht möglich, aber denkbar
  - Zugriff über Partneraccounts jetzt schon möglich, aber dadurch Zugriff auch auf andere Dienste (Verantwortlichkeit)
  - Jupyter Account kann über einen Link weitergegeben werden. Denkbar z.B. für eine Schulklasse, aber alle haben dann dasselbe Homeverzeichnis. Oder einen Partneraccount erstellen und weitergegeben
  - Hinweis 
    - Ähnliches Szenario sind Vorkurse in RWTHmoodle: es gibt Schnittstelle für lokale Moodle Accounts, diese werden nach 6 Monaten abgeräumt - evtl. auch für RWTHjupyter denkbar?
    - es gab einen L2P Einladungsprozess (Externe einladen, weniger Rechte als normale Accounts)
    - Datenschutzfragen beachten (ggf. muss Einwilligung eingeholt werden)
- Frage: Besteht die Möglichkeit "hidden profile" erst mit Vorlesungsstart auf public zu setzen? 
  - IT Center: Möglichkeit besteht 






