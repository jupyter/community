This flow chart describes the process for the implementation of new features in the RWHTjupyter cluster. It aims to allow for contributions by the RWTHjupyter community. The RWTHjuypyter board keeps track of open issues and feature requests and facilitates the collaboration between members of the community.
The community can submit change requests for changes to be applied in the cluster. These change requests are evaluated and approved by the board and subsequently implemented by the IT centers LBA group.

```mermaid
flowchart LR
    user((User))
    jupyter((Wider Jupyter<br>Community))
    community((RWTHjupyter<br>Community))
    itc-sd((ITC ServiceDesk))
    itc-lba((LBA Group))
    board((RWTHjupyter<br>Board))
    interest-group((Interest<br>Group))

    feature[Feature<br>Request]
    bug[Bug<br>Report]
    gitlab[GitLab<br>Issue]
    change-request[Change<br>Request]

    user -- creates --> feature
    user --> bug

    feature -- submitted --> itc-sd
    bug --> itc-sd

    itc-lba -- implements --> gitlab

    itc-sd -- creates --> gitlab
    board --> gitlab
    gitlab -- iterates monthly --> board

    board -- forms --> interest-group
    community -- joins --> interest-group

    jupyter --> community
    community -- collaborates --> jupyter

    interest-group -- proposes --> change-request
    board -- evaluates --> change-request

    change-request -. is tracked by .-> gitlab
```

The first project we're aiming at is the port of nbgrader to jupyterlab. The process of this port is being tracked in this  [Issue](https://git.rwth-aachen.de/jupyter/community/-/issues/22).
