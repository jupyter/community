# RWTHjupyter Community

## Präambel
Der Service RWTHjupyter ist ein Angebot für die RWTH dessen Wert durch den Input der Nutzenden und die kontinuierliche Entwicklung wesentlich bestimmt wird. Mit der Schaffung einer Community rund um die Nutzung des Service RWTHjupyter wird das Ziel verfolgt diese zu unterstützen, das Zusammenspiel der beteiligten Gruppen (Nutzende, Entwickelnde, Betreibende, Externe) zu fördern und zu organisieren sowie konkrete Aufgaben zu verfolgen.

## Aufgaben
Die Liste der Aufgaben konkretisiert die Zielsetzungen in der Community und soll kontinuierlich gepflegt werden:
* Organisation des Austauschs rund um den Service RWTHjupyter
* Steuerung des Service RWTHjupyter
  * Priorisierung von Bug- und Featurelisten
  * Erstellung von Prozessen zur Ressourcenvergabe in RWTHjupyter
  * Koordination und Nutzung von Synergieeffekten bei Entwicklungen für RWTHjupyter
  * Beratung bei Erstellung und Änderung der Nutzungsbedingungen
  * Beratung zu Finanzierungsfragen
* Austausch und Beteiligung an RWTH-externen Aktivitäten (z.B. Jupyter Community, andere Einrichtungen)
* Vermittlung bei Ressourcenbedarfen im RWTHjupyter-Kontext

## Organe
### RWTHjupyter Community
Als RWTHjupyter Community werden alle RWTH-Angehörigen mit Interesse an RWTHjupyter definiert. Diese können sich in der für die Kommunikation bereitgestellte Mailingliste ([jupyter@lists.rwth-aachen.de](https://lists.rwth-aachen.de/postorius/lists/jupyter.lists.rwth-aachen.de/)) registrieren, um über die Aktivitäten in der RWTHjupyter Community informiert zu werden. Die RWTHjupyter Community trifft sich einmal pro Semester, das Treffen wird durch das RWTHjupyter Board organisiert.

### RWTHjupyter Board
Das RWTHjupyter Board vertritt die Interessen der RWThjupyter Community. Seine Mitglieder stammen aus der RWTHjupyter Community mit dem Ziel diese möglichst gut abzubilden. Die Zugehörigkeit wird über die Mailingliste ([jupyter-board@lists.rwthaachen.de](url)) definiert. Das IT Center ist ständiges Mitglied des RWTHjupyter Boards und ist für die Organisation (Initiierung von Prozessen, Moderation, etc.) verantwortlich. Weitere Mitglieder werden nach Ermessen des RWTHjupyter Boards aufgenommen, die Anzahl der Mitglieder soll zehn Personen nicht überschreiten. Das RWTHjupyter Board trifft sich regelmäßig. Hauptaufgabe des RWHTjupyter Boards ist die Bearbeitung der im Abschnitt Aufgaben definierten Aufgaben. Das RWTHjuypter Board regelt seine Prozesse (Kommunikation, Dokumentation, etc.) selbst.

## Organisation
### Gründung
Für die Definition und Gründung der RWTHjupyter Community sind die Vertretungen des an der Bereitstellung der ersten Instanz des Service RWTHjupyter beteiligten Einrichtungen (ACS, LfB, IENT, IT Center) verantwortlich. Sie bilden auch das erste RWTHjupyter Board, das entsprechend der Regeln erweitert werden kann. Mit dem ersten Treffen der RWTHjupyter Community gilt diese als gegründet.
### Auflösung
Die Existenz dieses Gremiums ist mit dem Angebot des Service RWTHjupyter verknüpft. Wird der Service nicht mehr angeboten, so ist diese Gremium aufzulösen. Die Übernahme in einen anderen Kontext ist möglich und kann durch die beteiligten Stellen nach Möglichkeit unterstützt werden.
